# Teste SIN Solution

### Aplicação no Heroku:

https://teste-sin.herokuapp.com/

---

### Sobre a Aplicação:

Desenvolvida em Laravel 5.8 utilizando Repositories e Services para melhor organização das responsabilidades do código e boas práticas da linguagem PHP.

Foi utilizado o Vue.js para a criação de alguns webcomponents utilizando o recurso do Laravel Mix.

---

### Pré requisitos:

- PHP >= 7.1.3
- MySQL >= 5

---

### Instalando a aplicação:

- Copiar o `.env.example` para `.env`:

```bash
    > cp .env.example .env
```

- Instalação dos pacotes:

```bash
    > composer install
``` 

- Gerar chave da aplicação:

```bash
    > php artisan key:generate
``` 

- Executar as migrates (Criação das tabeas do Banco de Dados):

```bash
    > php artisan migrate
``` 

- Servir a aplicação:

```bash
    > php artisan serve
```

---

### Login da Aplicação:

Tanto para a aplicação local quanto para a hospedada no Heroku.

- E-mail: `admin@mail.com`
- Senha: `123`
