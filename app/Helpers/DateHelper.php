<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateHelper
{
    public static function formatDate($date, string $format = 'd/m/Y')
    {
        $date = Carbon::parse($date);
        return $date->format($format);
    }
}
