<?php

namespace App\Helpers;

class StringHelper
{
    public static function formatMoney($value)
    {
        return 'R$ ' . number_format($value, 2, ',', '.');
    }

    public static function formatLongText($text)
    {
        if (strlen($text) >= 70) {
            $text = substr($text, 0, 70) . '...';
        }

        return $text;
    }
}
