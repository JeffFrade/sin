<?php

namespace App\Http;

use App\Core\Support\Controller;
use App\Repositories\Models\Client as MClient;
use App\Services\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $client;
    private $mClient;

    public function __construct(Client $client, MClient $mClient)
    {
        $this->client = $client;
        $this->mClient = $mClient;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $clients = $this->client->index($params);

        return view('clients.index', compact('params', 'clients'));
    }

    public function create()
    {
        $client = $this->mClient;

        return view('clients.create', compact('client'));
    }

    public function store(Request $request)
    {
        $params = $this->toValidate($request, null);

        $this->client->store($params);

        return redirect(route('clients.index'));
    }

    public function edit($id)
    {
        $client = $this->client->edit($id);

        return view('clients.edit', compact('client'));
    }

    public function update(Request $request, $id)
    {
        $params = $this->toValidate($request, $id);
        $this->client->update($id, $params);

        return redirect(route('clients.index'));
    }

    public function delete($id)
    {
        return $this->client->delete($id);
    }

    protected function toValidate($request, $id)
    {
        return $this->validate($request, [
            'name' => 'required|max:70',
            'email' => 'required|max:100|unique:clients,email,' . $id,
            'zip' => 'required|max:8',
            'address' => 'required|max:100',
            'number' => 'required',
            'complement' => 'nullable|max:15',
            'neighborhood' => 'required|max:100',
            'city' => 'required|max:100',
            'state' => 'required|max:25'
        ]);
    }
}