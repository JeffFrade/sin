<?php

namespace App\Http;

use App\Core\Support\Controller;
use App\Repositories\ClientRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $clientRepository;
    private $productRepository;
    private $orderRepository;

    public function __construct(ClientRepository $clientRepository, ProductRepository $productRepository, OrderRepository $orderRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $clients = $this->clientRepository->countAll();
        $products = $this->productRepository->countAll();
        $orders = $this->orderRepository->countAll();

        return view('dashboard', compact('clients', 'products', 'orders'));
    }
}
