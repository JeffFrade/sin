<?php

namespace App\Http;

use App\Core\Support\Controller;
use App\Repositories\Models\Order as MOrder;
use App\Services\Client;
use App\Services\Order;
use App\Services\ProdOrder;
use App\Services\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $mOrder;
    private $order;
    private $product;
    private $prodOrder;
    private $client;

    public function __construct(Order $order, Product $product, ProdOrder $prodOrder, Client $client)
    {
        $this->product = $product;
        $this->prodOrder = $prodOrder;
        $this->client = $client;
        $this->order = $order;
        $this->mOrder = new MOrder();
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $orders = $this->order->index($params);

        return view('orders.index', compact('orders', 'params'));
    }

    public function create()
    {
        $order = $this->mOrder;
        $products = $this->product->getProductsOrdered();
        $clients = $this->client->getClientsOrdered();
        $prodOrder = [
            'products' => [],
            'amounts' => []
        ];

        return view('orders.create', compact('order', 'products', 'clients', 'prodOrder'));
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $order = $this->order->store($params);

        if (!$order['stats']) {
            return back()->withErrors(['msg' => $order['msgs']]);
        }

        return redirect(route('orders.index'));
    }

    public function edit($id)
    {
        $order = $this->order->edit($id);
        $products = $this->product->getProductsOrdered();
        $clients = $this->client->getClientsOrdered();
        $prodOrder = $this->prodOrder->getProdOrders($id);

        return view('orders.edit', compact('order', 'products', 'clients', 'prodOrder'));
    }

    public function update(Request $request, $id)
    {
        $params = $request->all();

        $order = $this->order->update($params, $id);

        if (!$order['stats']) {
            return back()->withErrors(['msg' => $order['msgs']]);
        }

        return redirect(route('orders.index'));
    }

    public function delete($id)
    {
        $this->order->delete($id);
    }
}
