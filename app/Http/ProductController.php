<?php

namespace App\Http;

use App\Core\Support\Controller;
use App\Services\Product;
use App\Repositories\Models\Product as MProduct;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $product;
    private $mProduct;

    public function __construct(Product $product, MProduct $mProduct)
    {
        $this->product = $product;
        $this->mProduct = $mProduct;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $products = $this->product->index($params);

        return view('products.index', compact('params', 'products'));
    }

    public function create()
    {
        $product = $this->mProduct;

        return view('products.create', compact('product'));
    }

    public function store(Request $request)
    {
        $params = $this->toValidate($request);

        $this->product->store($params);

        return redirect(route('products.index'));
    }

    public function edit($id)
    {
        $product = $this->product->edit($id);

        return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $params = $this->toValidate($request);
        $this->product->update($id, $params);

        return redirect(route('products.index'));
    }

    public function delete($id)
    {
        return $this->product->delete($id);
    }

    protected function toValidate($request)
    {
        return $this->validate($request, [
            'title' => 'required|max:200',
            'price' => 'required',
            'description' => 'required|max:5000',
        ]);
    }
}
