<?php

namespace App\Repositories;

use App\Repositories\Models\Client;

class ClientRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Client();
    }

    public function filter($search)
    {
        return $this->model->where('name', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->paginate();
    }

    public function getClientsOrdered()
    {
        return $this->model->orderBy('name')->get();
    }
}
