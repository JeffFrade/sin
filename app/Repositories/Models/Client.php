<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';
    protected $fillable = [
        'name',
        'email',
        'zip',
        'address',
        'number',
        'complement',
        'neighborhood',
        'city',
        'state'
    ];

    public function order()
    {
        return $this->hasMany(Order::class, 'id', 'id_client');
    }
}
