<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders';
    protected $fillable = [
        'id_client',
        'total'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class, 'id_client', 'id');
    }

    public function prodOrder()
    {
        return $this->hasMany(ProdOrder::class, 'id_order', 'id');
    }
}
