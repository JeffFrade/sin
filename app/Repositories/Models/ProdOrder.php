<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProdOrder extends Model
{
    use SoftDeletes;

    protected $table = 'prod_order';
    protected $fillable = [
        'id_order',
        'id_product',
        'amount'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'id_order', 'id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id_product', 'id');
    }
}
