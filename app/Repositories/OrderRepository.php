<?php

namespace App\Repositories;

use App\Repositories\Models\Order;

class OrderRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Order();
    }

    public function filter($search)
    {
        return $this->model->whereHas('client', function ($query) use ($search) {
            $query->where('name', 'LIKE', '%' . $search. '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%');
        })->paginate();
    }
}
