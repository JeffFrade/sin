<?php

namespace App\Repositories;

use App\Repositories\Models\ProdOrder;

class ProdOrderRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new ProdOrder();
    }

    public function getProdOrders($id)
    {
        return $this->model->where('id_order', $id)
            ->get(['id_product', 'amount'])
            ->toArray();
    }
}
