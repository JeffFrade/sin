<?php

namespace App\Repositories;

use App\Repositories\Models\Product;

class ProductRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Product();
    }

    public function filter($search)
    {
        return $this->model->where('title', 'like', '%' . $search . '%')
            ->orWhere('description', 'like', '%' . $search . '%')
            ->paginate();
    }

    public function getProductsOrdered()
    {
        return $this->model->orderBy('title')->get();
    }
}
