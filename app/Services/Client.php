<?php

namespace App\Services;

use App\Repositories\ClientRepository;
use App\Repositories\Models\Client as MClient;

class Client
{
    private $mClient;
    private $clientRepository;

    public function __construct()
    {
        $this->mClient = new MClient();
        $this->clientRepository = new ClientRepository();
    }

    public function index(array $params)
    {
        return $this->clientRepository->filter($params['search'] ?? '');
    }

    public function store(array $params)
    {
        return $this->clientRepository->create($params);
    }

    public function edit($id)
    {
        return $this->clientRepository->findFirst('id', $id);
    }

    public function update($id, $params)
    {
        return $this->clientRepository->update($params, $id);
    }

    public function delete($id)
    {
        return $this->clientRepository->delete($id);
    }

    public function getClientsOrdered()
    {
        return $this->clientRepository->getClientsOrdered();
    }
}