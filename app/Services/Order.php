<?php

namespace App\Services;

use App\Repositories\Models\Order as MOrder;
use App\Repositories\OrderRepository;
use App\Repositories\ProdOrderRepository;
use App\Repositories\ProductRepository;

class Order
{
    private $mOrder;
    private $orderRepository;
    private $prodOrderRepository;
    private $productRepository;

    public function __construct()
    {
        $this->mOrder = new MOrder();
        $this->orderRepository = new OrderRepository();
        $this->prodOrderRepository = new ProdOrderRepository();
        $this->productRepository = new ProductRepository();
    }

    public function index(array $params)
    {
        $search = $params['search'] ?? '';
        return $this->orderRepository->filter($search);
    }

    public function store(array $params)
    {
        $params['amount'] = array_filter($params['amount']);
        $msg = $this->toValidate($params);

        if (count($msg) > 0) {
            return [
                'stats' => false,
                'msgs' => $msg
            ];
        }

        $data = [
            'id_client' => $params['id_client'],
            'total' => 0
        ];

        $order = $this->orderRepository->create($data);

        $this->insertOrders($params, $order);

        return [
            'stats' => true
        ];
    }

    public function edit($id)
    {
        return $this->orderRepository->findFirst('id', $id);
    }

    public function update(array $params, $id)
    {
        $params['amount'] = array_filter($params['amount']);
        $msg = $this->toValidate($params);

        if (count($msg) > 0) {
            return [
                'stats' => false,
                'msgs' => $msg
            ];
        }

        $order = $this->orderRepository->findFirst('id', $id);

        $this->insertOrders($params, $order);

        return [
            'stats' => true
        ];
    }

    public function delete($id)
    {
        $this->prodOrderRepository->customDelete('id_order', $id);
        $this->orderRepository->delete($id);
    }

    public function insertOrders($params, $order)
    {
        $this->prodOrderRepository->customDelete('id_order', $order->id);

        $total = 0;

        foreach ($params['product'] as $item => $product) {
            $data = [
                'id_order' => $order->id,
                'id_product' => $item,
                'amount' => $params['amount'][$item]
            ];

            $productObj = $this->productRepository->findFirst('id', $item);

            $total += $productObj->price * $params['amount'][$item];

            $this->prodOrderRepository->create($data);
        }

        $data = [
            'total' => $total
        ];

        $this->orderRepository->update($data, $order->id);
    }

    public function toValidate(array $params)
    {
        $msg = [];
        if (!isset($params['id_client']) || empty($params['id_client'])) {
            $msg[] = 'O campo cliente é obrigatório.';
        }

        if (count($params['amount']) <= 0 || count($params['product']) <= 0) {
            $msg[] = 'Inserir ao menos 1 produto ou quantidade';
        }

        return $msg;
    }
}
