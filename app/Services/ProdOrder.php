<?php

namespace App\Services;

use App\Repositories\Models\ProdOrder as MProdOrder;
use App\Repositories\ProdOrderRepository;

class ProdOrder
{
    private $mProdOrder;
    private $prodOrderRepository;

    public function __construct()
    {
        $this->mProdOrder = new MProdOrder();
        $this->prodOrderRepository = new ProdOrderRepository();
    }

    public function getProdOrders($id)
    {
        $prodOrders = $this->prodOrderRepository->getProdOrders($id);
        $po = [];

        foreach ($prodOrders as $prodOrder) {
            $po['products'][] = $prodOrder['id_product'];
            $po['amounts'][$prodOrder['id_product']] = $prodOrder['amount'];
        }

        return $po;
    }
}
