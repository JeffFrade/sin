<?php

namespace App\Services;

use App\Repositories\Models\Product as MProduct;
use App\Repositories\ProductRepository;

class Product
{
    private $mProduct;
    private $productRepository;

    public function __construct()
    {
        $this->mProduct = new MProduct();
        $this->productRepository = new ProductRepository();
    }

    public function index(array $params)
    {
        return $this->productRepository->filter($params['search'] ?? '');
    }

    public function store(array $params)
    {
        return $this->productRepository->create($params);
    }

    public function edit($id)
    {
        return $this->productRepository->findFirst('id', $id);
    }

    public function update($id, $params)
    {
        return $this->productRepository->update($params, $id);
    }

    public function delete($id)
    {
        return $this->productRepository->delete($id);
    }

    public function getProductsOrdered()
    {
        return $this->productRepository->getProductsOrdered();
    }
}
