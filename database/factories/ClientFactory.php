<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Repositories\Models\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'zip'=> rand(11111111, 99999999),
        'address' => $faker->address,
        'number' => rand(1, 50000),
        'complement' => null,
        'neighborhood' => $faker->title,
        'city' => $faker->city,
        'state' => $faker->title
    ];
});
