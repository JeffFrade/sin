<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Repositories\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'id_client' => rand(1, 5),
        'total' => rand(1, 30000),
    ];
});
