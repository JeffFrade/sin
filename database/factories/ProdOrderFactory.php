<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Repositories\Models\ProdOrder;
use Faker\Generator as Faker;

$factory->define(ProdOrder::class, function (Faker $faker) {
    return [
        'id_order' => rand(1, 3),
        'id_product' => rand(1, 20),
        'amount' => rand(1, 10)
    ];
});
