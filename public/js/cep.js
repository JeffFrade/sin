let cep = (cep) => {
    $.ajax({
        method: 'GET',
        url: 'https://viacep.com.br/ws/' + cep + '/json',
        timeout: 0,
        dataType:'json',

        success: function (response) {
            $('#address').val(response.logradouro);
            $('#neighborhood').val(response.bairro);
            $('#city').val(response.localidade);
            $('#state').val(response.uf);
        }
    });
}