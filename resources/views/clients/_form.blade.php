@include('util.errors')

<div class="col-sm-6">
    <div class="form-group">
        <label for="name"><span class="required">*</span> Nome:</label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Nome" value="{{ old('name', $client->name) }}">
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="email"><span class="required">*</span> E-mail:</label>
        <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" value="{{ old('email', $client->email) }}">
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <label for="zip"><span class="required">*</span> CEP:</label>
        <input type="text" id="zip" name="zip" class="form-control" placeholder="CEP" value="{{ old('zip', $client->zip) }}" max="11">
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <label for="address"><span class="required">*</span> Endereço:</label>
        <input type="text" id="address" name="address" class="form-control" placeholder="Endereço" value="{{ old('address', $client->endereco) }}">
    </div>
</div>

<div class="col-sm-2">
    <div class="form-group">
        <label for="number"><span class="required">*</span> Número:</label>
        <input type="number" id="number" name="number" class="form-control" placeholder="Número" value="{{ old('number', $client->number) }}">
    </div>
</div>

<div class="col-sm-2">
    <div class="form-group">
        <label for="complement">Complemento:</label>
        <input type="text" id="complement" name="complement" class="form-control" placeholder="Complemento" value="{{ old('complement', $client->complement) }}">
    </div>
</div>


<div class="col-sm-4">
    <div class="form-group">
        <label for="neighborhood"><span class="required">*</span> Bairro:</label>
        <input type="text" id="neighborhood" name="neighborhood" class="form-control" placeholder="Bairro" value="{{ old('neighborhood', $client->neighborhood) }}">
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <label for="city"><span class="required">*</span> Cidade:</label>
        <input type="text" id="city" name="city" class="form-control" placeholder="Cidade" value="{{ old('city', $client->city) }}">
    </div>
</div>

<div class="col-sm-4">
    <div class="form-group">
        <label for="state"><span class="required">*</span> Estado:</label>
        <input type="text" id="state" name="state" class="form-control" placeholder="Estado" value="{{ old('state', $client->state) }}">
    </div>
</div>
<div class="col-sm-12">
    <span class="required">*</span> Obrigatório
</div>