
@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Clientes</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['route' => 'clients.index', 'method' => 'GET']) }}
            <box type="success">
                <template slot="header">
                    <div class="col-sm-8">
                        <input type="text" id="search" name="search" class="form-control" placeholder="Nome/E-mail" value="{{ $params['search'] ?? '' }}">
                    </div>

                    <div class="col-sm-4">
                        <button class="btn btn-success btn-filter"><i class="fa fa-search"></i> Filtrar</button>
                        &nbsp;
                        <a href="{{ route('clients.create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Cadastrar Cliente</a>
                    </div>
                </template>

                <template slot="body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th nowrap="" style="width: 1%">Ações</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse($clients as $client)
                                <tr>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td nowrap="" style="width: 1%">
                                        <a href="{{ route('clients.edit', ['id' => $client->id]) }}" class="btn btn-default btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
                                        &nbsp;
                                        <a href="#" class="btn btn-danger btn-xs btn-del" data-id="{{ $client->id }}" title="Deletar"><i class="fa fa-ban"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3">Não há dados</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </template>

                <template slot="footer">
                    <div class="center-paginate">
                        {!! \App\Helpers\PaginateHelper::paginateWithParams($clients, $params) !!}
                    </div>
                </template>
            </box>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('css')
    <style type="text/css">
        .center-paginate {
            text-align: center;
        }

        .pagination > .active > a,
        .pagination > .active > a:focus,
        .pagination > .active > a:hover,
        .pagination > .active > span,
        .pagination > .active > span:focus,
        .pagination > .active > span:hover {
            background-color: #00a65a;
            border-color: #00a65a;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $('.btn-del').on('click', function (e) {
            e.preventDefault();
            $('.overlay').removeClass('hidden');

            if (confirm('Deseja deletar o cliente?')) {
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',

                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'DELETE',

                    url: '/dashboard/clients/delete/' + $(this).data('id'),
                    timeout: 0,

                    success: function (response) {
                        location.reload();
                    }
                });
            } else {
                $('.overlay').addClass('hidden');
            }
        });

        $('.btn-filter').on('click', function () {
            $('.overlay').removeClass('hidden');
        });
    </script>
@stop