@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <info-box color="green" icon="fa fa-users" title="Clientes" value="{{ $clients }}"></info-box>
        </div>

        <div class="col-sm-4">
            <info-box color="green" icon="fa fa-tags" title="Produtos" value="{{ $products }}"></info-box>
        </div>

        <div class="col-sm-4">
            <info-box color="green" icon="fa fa-dollar" title="Pedidos" value="{{ $orders }}"></info-box>
        </div>
    </div>
@stop