@include('util.errors')

<div class="col-xs-12">
    <div class="form-group">
        <label for="id_client"><span class="required">*</span> Cliente:</label>
        <select id="id_client" name="id_client" class="form-control">
            <option value="" selected="" disabled="">Selecione um Cliente</option>
            @forelse($clients as $client)
                <option value="{{ $client->id }}" {{ (old('id_client', $order->id_client) == $client->id?'selected="selected"':'') }}>{{ $client->name }}</option>
            @empty
                <option value=""></option>
            @endforelse
        </select>
    </div>
</div>

<label><span class="required">*</span> Produto:</label>
<div class="col-xs-12 products">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th class="text-center" nowrap="" style="width: 1%">Selecionado</th>
                <th class="text-center">Produto</th>
                <th class="text-center">Valor</th>
                <th class="text-center" nowrap="" style="width: 1%">Quantidade</th>
            </tr>
        </thead>

        <tbody>
            @forelse($products as $product)
                <tr>
                    <td class="text-center nowrap=" style="width: 1%"><input type="checkbox" id="product[{{ $product->id }}]" name="product[{{ $product->id }}]" title="Selecionar" {{ (in_array($product->id, $prodOrder['products'])?'checked="checked"':'') }}></td>
                    <td>{{ $product->title }}</td>
                    <td>{{ \App\Helpers\StringHelper::formatMoney($product->price) }}</td>
                    <td class="text-center nowrap=" style="width: 1%"><input type="number" id="amount[{{ $product->id }}]" name="amount[{{ $product->id }}]" class="form-control" min="0" {{ (in_array($product->id, array_keys($prodOrder['amounts']))?'value=' . $prodOrder['amounts'][$product->id]:'') }}></td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Não há dados</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>