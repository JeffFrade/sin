@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Cadastrar Pedido</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['route' => 'orders.store', 'method' => 'POST']) }}
            <box type="success">
                <template slot="header">
                    <h3 class="box-title">Cadastro de Pedido</h3>
                </template>

                <template slot="body" id="zip">
                    @include('orders._form')
                </template>

                <template slot="footer">
                    <a href="{{ route('orders.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                    <button class="btn btn-success btn-save pull-right"><i class="fa fa-save"></i> Salvar</button>
                </template>
            </box>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('css')
    <style type="text/css">
        textarea {
            resize: none;
        }

        .required {
            color: #f00;
            font-weight: bold;
        }

        .products {
            height: 350px;
            overflow-y: auto;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $('.btn-save').on('click', function () {
            $('.overlay').removeClass('hidden');
        });

        $('.form-error').each(function (index) {
            $.notify({message: $(this).text()}, {type: 'danger'});
        });

        $(':input[type="checkbox"]').on('change', function () {
            $(this).parents('tr').find(':input[type="number"]').val(1);
        })
    </script>
@stop
