
@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Pedidos</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['route' => 'orders.index', 'method' => 'GET']) }}
            <box type="success">
                <template slot="header">
                    <div class="col-sm-8">
                        <input type="text" id="search" name="search" class="form-control" placeholder="Título/Descrição" value="{{ $params['search'] ?? '' }}">
                    </div>

                    <div class="col-sm-4">
                        <button class="btn btn-success btn-filter"><i class="fa fa-search"></i> Filtrar</button>
                        &nbsp;
                        <a href="{{ route('orders.create') }}" class="btn btn-default"><i class="fa fa-plus"></i> Cadastrar Pedido</a>
                    </div>
                </template>

                <template slot="body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Cliente</th>
                                <th>E-mail</th>
                                <th>Valor Total</th>
                                <th>Data do Pedido</th>
                                <th nowrap="" style="width: 1%">Ações</th>
                            </tr>
                        </thead>

                        <tbody>
                            @forelse($orders as $order)
                                <tr>
                                    <td>{{ $order->client->name }}</td>
                                    <td>{{ $order->client->email }}</td>
                                    <td>{{ \App\Helpers\StringHelper::formatMoney($order->total) }}</td>
                                    <td>{{ \App\Helpers\DateHelper::formatDate($order->created_at) }}</td>
                                    <td nowrap="" style="width: 1%">
                                        <a href="{{ route('orders.edit', ['id' => $order->id]) }}" class="btn btn-default btn-xs" title="Editar"><i class="fa fa-edit"></i></a>
                                        &nbsp;
                                        <a href="#" class="btn btn-danger btn-xs btn-del" data-id="{{ $order->id }}" title="Deletar"><i class="fa fa-ban"></i></a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="5">Não há dados</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </template>

                <template slot="footer">
                    <div class="center-paginate">
                        {{ \App\Helpers\PaginateHelper::paginateWithParams($orders, []) }}
                    </div>
                </template>
            </box>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('css')
    <style type="text/css">
        .center-paginate {
            text-align: center;
        }

        .pagination > .active > a,
        .pagination > .active > a:focus,
        .pagination > .active > a:hover,
        .pagination > .active > span,
        .pagination > .active > span:focus,
        .pagination > .active > span:hover {
            background-color: #00a65a;
            border-color: #00a65a;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $('.btn-del').on('click', function (e) {
            e.preventDefault();
            $('.overlay').removeClass('hidden');

            if (confirm('Deseja deletar o pedido?')) {
                $.ajax({
                    contentType: 'application/x-www-form-urlencoded',

                    data: {
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'DELETE',

                    url: '/dashboard/orders/delete/' + $(this).data('id'),
                    timeout: 0,

                    success: function (response) {
                        location.reload();
                    }
                });
            } else {
                $('.overlay').addClass('hidden');
            }
        });

        $('.btn-filter').on('click', function () {
            $('.overlay').removeClass('hidden');
        });
    </script>
@stop