@include('util.errors')
<div class="col-sm-9">
    <div class="form-group">
        <label for="title"><span class="required">*</span> Título:</label>
        <input type="text" id="title" name="title" class="form-control" placeholder="Título" value="{{ old('title', $product->title) }}">
    </div>
</div>

<div class="col-sm-3">
    <div class="form-group">
        <label for="price"><span class="required">*</span> Preço:</label>
        <input type="number" step="0.01" min="0" id="price" name="price" class="form-control" placeholder="Preço" value="{{ old('price', $product->price) }}">
    </div>
</div>

<div class="col-sm-12">
    <div class="form-group">
        <label for="description"><span class="required">*</span> Descrição:</label>
        <textarea id="description" name="description" rows="5" class="form-control" placeholder="Descrição">{{ old('description', $product->description) }}</textarea>
    </div>
</div>

<div class="col-sm-12">
    <span class="required">*</span> Obrigatório
</div>