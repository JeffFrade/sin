@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Editar Produto</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            {{ Form::open(['route' => ['products.update', $product->id], 'method' => 'PUT']) }}
            <box type="success">
                <template slot="header">
                    <h3 class="box-title">Editar Produto</h3>
                </template>

                <template slot="body">
                    @include('products._form')
                </template>

                <template slot="footer">
                    <a href="{{ route('products.index') }}" class="btn btn-danger"><i class="fa fa-times"></i> Cancelar</a>
                    <button class="btn btn-success btn-save pull-right"><i class="fa fa-save"></i> Salvar</button>
                </template>
            </box>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('css')
    <style type="text/css">
        textarea {
            resize: none;
        }

        .required {
            color: #f00;
            font-weight: bold;
        }
    </style>
@stop

@section('js')
    <script type="text/javascript">
        $('.btn-save').on('click', function () {
            $('.overlay').removeClass('hidden');
        });

        $('.form-error').each(function (index) {
            $.notify({message: $(this).text()}, {type: 'danger'});
        });
    </script>
@stop