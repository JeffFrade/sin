<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'products'], function () {
        Route::get('/', 'ProductController@index')->name('products.index');
        Route::get('/create', 'ProductController@create')->name('products.create');
        Route::post('/store', 'ProductController@store')->name('products.store');
        Route::get('/edit/{id}', 'ProductController@edit')->name('products.edit');
        Route::put('/update/{id}', 'ProductController@update')->name('products.update');
        Route::delete('/delete/{id}', 'ProductController@delete')->name('products.delete');
    });

    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientController@index')->name('clients.index');
        Route::get('/create', 'ClientController@create')->name('clients.create');
        Route::post('/store', 'ClientController@store')->name('clients.store');
        Route::get('/edit/{id}', 'ClientController@edit')->name('clients.edit');
        Route::put('/update/{id}', 'ClientController@update')->name('clients.update');
        Route::delete('/delete/{id}', 'ClientController@delete')->name('clients.delete');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'OrderController@index')->name('orders.index');
        Route::get('/create', 'OrderController@create')->name('orders.create');
        Route::post('/store', 'OrderController@store')->name('orders.store');
        Route::get('/edit/{id}', 'OrderController@edit')->name('orders.edit');
        Route::put('/update/{id}', 'OrderController@update')->name('orders.update');
        Route::delete('/delete/{id}', 'OrderController@delete')->name('orders.delete');
    });
});
